# Back-end App Test


  ### Create Database and Populate table

1. Go to psql command line

```

CREATE DATABASE test;

```

2. Connect to the database you created

```

\c test;

```

3. Create tables in database (must use forward / slash)

```

\i C:/Users/OngHanSheng/Desktop/test/database/schema.sql;

```

### For Client & Server:
1. Navigate to the test folder (../test/backend/App/). Install the required dependencies 
```

npm install

```

2. Run in command prompt to start server

```

node ./bin/www

```

3. Go to browser localhost:3000
4. Follows the instruction in page shown in step 3.






Back-end application development task. Back-end app should be developed in express.js framework. Database can be used SQL / NoSQL (Postgress, MySQL, MongoDB, InfluxDB, etc). Coding should be done using "camelCase" standard. Each function & business logic should have proper commenting.

  

**NOTE:**

- Don't push the code in the master branch. Fork the branch and then start developing the codebase.

- Don't add node_modules directory

- create "backend" directory for Back-End codebase

- create "database" directory for datatabase file

- Nice to have feature - Docker & docker-compose.yml file [ Build and generate artifacts ]

- Add examples of how to use the APIs in your project's README file

  

**Problem Statement:**

Design and develop a back-end application which should return an Restful API for employee management.

  

**API List:**

1.  `/login` API that checks if the username && password is correct and returns a token, this token must be given in the header of subsequent APIs and validated before returning response

2.  `/getEmployee/:e_id` API that provides detail of given employee_id

3.  `/getEmployees` API that provides list of all employees


