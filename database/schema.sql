-- Account
CREATE TABLE IF NOT EXISTS account (
    id VARCHAR(100) PRIMARY KEY,
    password VARCHAR(32)
);

-- Employee
CREATE TABLE IF NOT EXISTS employee (
    id INTEGER PRIMARY KEY,
    name VARCHAR(100)
);

-- Insert Dummy Data

INSERT INTO account(id, password) VALUES ('login123@gmail.com', 'password');

INSERT INTO employee(id, name) VALUES (1, 'John');
INSERT INTO employee(id, name) VALUES (2,'Mary');
INSERT INTO employee(id, name) VALUES (3,'Elis');
INSERT INTO employee(id, name) VALUES (4,'Peter');
INSERT INTO employee(id, name) VALUES (5, 'Zero');
INSERT INTO employee(id, name) VALUES (6,'Jerry');

