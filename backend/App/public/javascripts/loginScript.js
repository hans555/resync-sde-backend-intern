function check(event) {
	// Get Values
	var email  = document.getElementById('email' ).value;
	var password    = document.getElementById('password'   ).value;
	
	// Simple Check
	if(email.length == 0) {
		alert("Invalid Email");
		event.preventDefault();
		event.stopPropagation();
		return false;
	}
	if(password.length == 0) {
		alert("Invalid Password");
		event.preventDefault();
		event.stopPropagation();
		return false;
	}
}