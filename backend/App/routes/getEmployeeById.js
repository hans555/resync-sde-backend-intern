var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var localStorage = require('localStorage');

var pool = require('../db');

/* SQL Query */
var sql_query = 'SELECT * FROM employee WHERE employee.id = $1';

router.get('/:e_id', verifyToken, function(req, res, next) {
	//verify the token with the key
	jwt.verify(req.token, 'secretkey', (err, authData) => {
		if(err) {
			res.sendStatus(403);
		} else {
			const values = [req.params.e_id.toString()];
			pool.query(sql_query, values, (err, data) => {
			res.render('getEmployeeById', { title: 'Result', data: data.rows });
			});
		}
	});
});

//Check if token is in the local storage
function verifyToken(req, res, next) {
	const token = localStorage.getItem("token");
	//Check if token is null
	if (token !== null) {
		req.token = token;
		// Next middleware
		next();
	} else {
		// Forbidden
		res.sendStatus(403);
	}
}

module.exports = router;
