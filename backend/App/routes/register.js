var express = require('express');
var router = express.Router();

var pool = require('../db');

router.get('/', function(req, res, next) {
  res.render('register', { title: 'Register' });
});

router.post('/', function (req, res, next) {
    // Insert Information
	var email  = req.body.email;
	var password = req.body.password;
	
	// Construct Specific SQL Query
	var insert_query = 'INSERT INTO account VALUES($1, $2)';
	
	pool.query(insert_query, [email, password] ,(err, data) => {
            if (err) {
                res.send("This email is taken")
            }
			res.redirect('/login')
	});
});



module.exports = router;