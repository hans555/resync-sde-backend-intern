var express = require('express');
var router = express.Router();
var localStorage = require('localStorage');
var jwt = require('jsonwebtoken');
var pool = require('../db');

/* SQL Query */
var sql_query = 'SELECT * FROM employee';

router.get('/', verifyToken ,function(req, res, next) {
	//verify the token with the key
	jwt.verify(req.token, 'secretkey', (err, authData) => {
		if(err) {
			res.sendStatus(403);
		} else {
			pool.query(sql_query, (err, data) => {
				res.render('getEmployees', { title: 'Employee Table', data: data.rows });
			});
		}
	});
});

//Check if token is in the local storage
function verifyToken(req, res, next) {
	const token = localStorage.getItem("token");
	//Check if token is null
	if (token !== null) {
		req.token = token;
		// Next middleware
		next();
	} else {
		// Forbidden
		res.sendStatus(403);
	}
}

module.exports = router;
