var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var pool = require('../db');
var localStorage = require('localStorage');

//Remove the token in the local storage
router.get('/', function (req, res, next) {
    localStorage.setItem('token', null);
    res.send('log out successfully');
});



module.exports = router;