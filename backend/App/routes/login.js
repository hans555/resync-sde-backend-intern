var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var pool = require('../db');
var localStorage = require('localStorage');

router.get('/', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.post('/', function (req, res, next) {
  // Retrieve Information
	var email  = req.body.email;
	var password = req.body.password;
	// Construct Specific SQL Query
	var select_query = "SELECT * FROM account WHERE id = $1 and password = $2";

	pool.query(select_query, [email, password] ,(err, data) => {
		if (data.rows.length == 0) {
			res.send("wrong email or password")
		} else {
			//Store the token in local storage
			const user = data.rows[0];
			jwt.sign({user}, 'secretkey', (err, token) => {
				localStorage.setItem('token', token);
			});
			res.redirect('/index')
		}
	});
});



module.exports = router;